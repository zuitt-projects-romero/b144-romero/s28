console.log(fetch('https://jsonplaceholder.typicode.com/todos'))

// No.3
fetch('https://jsonplaceholder.typicode.com/todos')
.then(res => res.json())
.then(data => {
	console.log(data)

// No.4 
let maplist = data.map(title => ({title:title.title}))
console.log(maplist)
})

// No. 5 and 6
fetch('https://jsonplaceholder.typicode.com/todos/1')
.then(res => res.json())
.then(data => {
console.log(data)

	console.log(`The item "${data.title}" on the list has a status of ${data.completed}`)

})

// No. 7 - 9

fetch('https://jsonplaceholder.typicode.com/todos/1', {
    method: 'PUT',
    headers: {
        'Content-Type': 'application/json'
    },
    body: JSON.stringify({
        title: "Groceries",
        description: "Vegetable and Meat",
        status: "pending",
        dateCompleted: "pending",
        userId: 1
    })
})
.then(response => response.json())
.then(data => console.log(data))

// No. 10
fetch('https://jsonplaceholder.typicode.com/todos/1', {
    method: 'PATCH',
    headers: {
        'Content-Type': 'application/json'
    },
    body: JSON.stringify({
   
        completed: true,
        dateCompleted: "12/03/21"
    })
})
.then(response => response.json())
.then(data => console.log(data))


fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'DELETE'
})
.then(res=> res.json())
.then(data=>console.log(data))